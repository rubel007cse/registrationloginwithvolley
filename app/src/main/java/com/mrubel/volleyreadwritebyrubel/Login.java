package com.mrubel.volleyreadwritebyrubel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    EditText login_name, login_password;
    ListView lvps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login_name = (EditText) findViewById(R.id.loginname);
        login_password = (EditText) findViewById(R.id.loginpassword);

        lvps = (ListView) findViewById(R.id.lvps);

        // loading data
        // from server code

        String tag_json_arry = "json_array_req";

        String url = "http://mrubel.com/api/volleyjson/logindata.php";


        // requesting an array from server
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("MyTag", response.toString());

                        // got the data in 'response'
                        try {
                            // Parsing json array response
                            // loop through each json object
                            String jsonResponse="";
                            // took an array save all data
                            final String[] all_data = new String[response.length()];

                            // looping through response to get all object
                            for (int i = 0; i < response.length(); i++) {

                                // recieving objects into person JSONobject
                                JSONObject person = (JSONObject) response.get(i);

                                //getting all data into variable by database column name from json data
                                String id = person.getString("id");
                                String name = person.getString("name");
                                String email = person.getString("email");
                                String phone = person.getString("phone");

                                // saving all data in a line into jsonResponse
                                jsonResponse += "Id: " + id + "\n\n";
                                jsonResponse += "Name: " + name + "\n\n";
                                jsonResponse += "Email: " + email + "\n\n";
                                jsonResponse += "Phone: " + phone + "\n\n";

                                //keeping all data into all_data array
                                all_data[i] = jsonResponse;

                                Log.d("data", ""+all_data[i]);

                            }

                            // shoowing data into list view
                            ArrayAdapter adaptes = new ArrayAdapter(getApplicationContext(), R.layout.mylaout, R.id.tv, all_data);
                            lvps.setAdapter(adaptes);


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Volley Error Tag", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        // Adding request to request queue
        com.mrubel.volleyreadwritebyrubel.AppController.getInstance().addToRequestQueue(req);

        Toast.makeText(getApplicationContext(), "Data loaded sucessfully!", Toast.LENGTH_LONG).show();








    }

    public void regPage(View b){
        Intent i = new Intent(Login.this, MainActivity.class);
        startActivity(i);

    }


    public void loginMyMethod(View v) {


            // Tag used to cancel the request
            String tag_string_req = "req_register";

            StringRequest strReq = new StringRequest(Request.Method.POST,
                    "http://mrubel.com/api/volleyjson/loginmain.php", new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.d("MyTag", "Register Response: " + response.toString());

                    if(response.toString().equals("success")){

                        Toast.makeText(getApplicationContext(), "Status "+response.toString(), Toast.LENGTH_LONG).show();

                        Intent o = new Intent(Login.this, Home.class);
                        startActivity(o);
                    } else {

                        Toast.makeText(getApplicationContext(), "Login Status "+response.toString(), Toast.LENGTH_LONG).show();

                    }



                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("MyTag", "Registration Error: " + error.getMessage());
                    Toast.makeText(getApplicationContext(),
                            error.getMessage(), Toast.LENGTH_LONG).show();

                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    // Posting params to register url
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("myname", login_name.getText().toString());
                    params.put("mypassword", login_password.getText().toString());


                    return params;
                }

            };

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

            }



    }

